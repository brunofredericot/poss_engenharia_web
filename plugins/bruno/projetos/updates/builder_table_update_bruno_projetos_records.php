<?php namespace Bruno\Projetos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrunoProjetosRecords extends Migration
{
    public function up()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->text('foto1');
            $table->text('foto2');
        });
    }
    
    public function down()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->dropColumn('foto1');
            $table->dropColumn('foto2');
        });
    }
}
