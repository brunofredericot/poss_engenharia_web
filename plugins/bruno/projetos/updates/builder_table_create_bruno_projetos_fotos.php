<?php namespace Bruno\Projetos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrunoProjetosFotos extends Migration
{
    public function up()
    {
        Schema::create('bruno_projetos_fotos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('titulo', 45);
            $table->string('legenda', 255)->nullable();
            $table->integer('order')->nullable()->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bruno_projetos_fotos');
    }
}
