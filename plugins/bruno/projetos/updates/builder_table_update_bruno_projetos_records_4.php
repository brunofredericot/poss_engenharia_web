<?php namespace Bruno\Projetos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrunoProjetosRecords4 extends Migration
{
    public function up()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->dropColumn('foto1');
            $table->dropColumn('foto2');
        });
    }
    
    public function down()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->string('foto1', 255);
            $table->string('foto2', 255)->nullable();
        });
    }
}
