<?php namespace Bruno\Projetos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrunoProjetosRecords5 extends Migration
{
    public function up()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->string('categoria', 45);
        });
    }
    
    public function down()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->dropColumn('categoria');
        });
    }
}
