<?php namespace Bruno\Projetos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrunoProjetosFotos2 extends Migration
{
    public function up()
    {
        Schema::table('bruno_projetos_fotos', function($table)
        {
            $table->text('foto');
        });
    }
    
    public function down()
    {
        Schema::table('bruno_projetos_fotos', function($table)
        {
            $table->dropColumn('foto');
        });
    }
}
