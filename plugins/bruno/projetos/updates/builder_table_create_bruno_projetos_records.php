<?php namespace Bruno\Projetos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrunoProjetosRecords extends Migration
{
    public function up()
    {
        Schema::create('bruno_projetos_records', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('titulo', 150);
            $table->string('cliente', 45);
            $table->text('descricao');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bruno_projetos_records');
    }
}
