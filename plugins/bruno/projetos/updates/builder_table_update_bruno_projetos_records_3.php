<?php namespace Bruno\Projetos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrunoProjetosRecords3 extends Migration
{
    public function up()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->string('foto1', 255)->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('foto2', 255)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->text('foto1')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('foto2')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
