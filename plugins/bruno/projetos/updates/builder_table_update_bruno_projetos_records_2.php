<?php namespace Bruno\Projetos\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrunoProjetosRecords2 extends Migration
{
    public function up()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->text('foto2')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('bruno_projetos_records', function($table)
        {
            $table->text('foto2')->nullable(false)->change();
        });
    }
}
