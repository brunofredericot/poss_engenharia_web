<?php namespace Bruno\Projetos\Models;

use Model;

/**
 * Model
 */
class Projetos extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bruno_projetos_records';


    /* Relacionamentos */

    public $attachOne = [

        'foto1' => 'System\Models\File',

    ];

    public $attachMany = [

        'galeria' => 'System\Models\File',

    ];   


}