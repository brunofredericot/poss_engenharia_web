<?php namespace Bruno\Clientes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrunoClientesLista extends Migration
{
    public function up()
    {
        Schema::create('bruno_clientes_lista', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('cliente', 45);
            $table->integer('ordem')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bruno_clientes_lista');
    }
}
