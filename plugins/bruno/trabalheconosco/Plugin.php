<?php namespace Bruno\TrabalheConosco;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
			'Bruno\TrabalheConosco\Components\TrabalheConosco' => 'trabalheconosco',
    	];
    }

    public function registerSettings()
    {
    }
}
