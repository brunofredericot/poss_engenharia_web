<?php namespace Bruno\TrabalheConosco\Components;

use Cms\Classes\ComponentBase;
use Input;
use Mail;
use Validator;
use Redirect;

class TrabalheConosco extends ComponentBase
{

    public function componentDetails(){
        return [
            'name' => 'Formulário de cadastro de vagas',
            'description' => 'Simples formulário de cadastro de vagas'
        ];
    }

    public function onSend()
    {
        $validator = Validator::make(
            [
                'nome' => Input::get('nome'),
                'email' => Input::get('email'),
                'telefone' => Input::get('telefone'),
                'vaga' => Input::get('vaga'),
                'assunto' => Input::get('assunto'),
                'mensagem' => Input::get('mensagem'),
            ],
            [
                'nome' => 'required|min:5',
                'telefone' => 'required|min:5',
                'vaga' => 'required|min:5',
                'email' => 'required|email',
                'assunto' => 'required|min:5',
                'mensagem' => 'required|min:5',
            ]
        );

        if($validator->fails()){

            return Redirect::back()->withErrors($validator);

        } else {
        
            $vars = [
                'nome'      => Input::get('nome'), 
                'email'     => Input::get('email'), 
                'vaga'     => Input::get('vaga'), 
                'telefone'  => Input::get('telefone'),
                'assunto'   => Input::get('assunto'),
                'mensagem'  => Input::get('mensagem'),
            ];


            Mail::send('contato.recebimento', $vars, function($message) {

                $message->to('contato@possengenharia.com.br', 'Novo currículo enviado pelo site');
                $message->subject('Currículo vindo do site');

            });

        }

    }

}