<?php namespace Bruno\Vagas\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Vagas extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'admin.vagas' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Bruno.Vagas', 'vagas', 'vagas_item');
    }
}