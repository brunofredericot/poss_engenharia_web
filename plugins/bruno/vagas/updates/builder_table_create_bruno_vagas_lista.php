<?php namespace Bruno\Vagas\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrunoVagasLista extends Migration
{
    public function up()
    {
        Schema::create('bruno_vagas_lista', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('titulo', 45);
            $table->integer('quantidade')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bruno_vagas_lista');
    }
}
