<?php namespace Bruno\Contato\Components;

use Cms\Classes\ComponentBase;
use Input;
use Mail;
use Validator;
use Redirect;

class ContactForm extends ComponentBase
{

    public function componentDetails() {
        return [
            'name' => 'Formulário de contato',
            'description' => 'Simples formulário de contato'
        ];
    }

    public function onSend()
    {
        $validator = Validator::make(
            [
                'nome' => Input::get('nome'),
                'email' => Input::get('email'),
                'assunto' => Input::get('assunto'),
                'mensagem' => Input::get('mensagem'),
            ],
            [
                'nome' => 'required|min:5',
                'email' => 'required|email',
                'assunto' => 'required|min:5',
                'mensagem' => 'required|min:5',
            ]
        );

        if($validator->fails()) {

            return Redirect::back()->withErrors($validator);

        } else {
        
            $vars = [
                'nome'      => Input::get('nome'), 
                'email'     => Input::get('email'), 
                'telefone'  => Input::get('telefone'),
                'assunto'   => Input::get('assunto'),
                'mensagem'  => Input::get('mensagem'),
            ];

            // Mail::send('contato.recebimento', $vars, function($message) {

            //     $message->to('contato@possengenharia.com.br', 'Contato Poss Engenharia');
            //     $message->subject('Contato vindo do site');

            // });

        }

    }

}