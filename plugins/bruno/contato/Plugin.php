<?php namespace Bruno\Contato;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'Bruno\Contato\Components\ContactForm' => 'contactform',    		
    	];
    }

    public function registerSettings()
    {
    }
}
