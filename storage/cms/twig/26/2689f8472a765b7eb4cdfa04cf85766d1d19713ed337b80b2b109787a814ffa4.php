<?php

/* /var/www/poss-engenharia/web/plugins/bruno/contato/components/contactform/default.htm */
class __TwigTemplate_fdc1f5879e365affb03cf1af800898cefea2c14e25fdba554ff81df258e37f92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"about-us\" class=\"page_contato\">

    <div class=\"container\">

        <div class=\"center wow fadeInDown\">
            <h2><img src=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Entre em contato conosco</h2>
        </div>

        <div class=\"row wow fadeInDown\" style=\"padding: 40px;\">
            
            <div class=\"col-md-10  col-md-offset-2 formulario\">
            
                <div class=\"col-md-6\" style=\"padding-right: 80px;\">

                    <h2>Preencha o formulário abaixo</h2>
                    ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "all", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 17
            echo "                        
                        ";
            // line 18
            echo twig_escape_filter($this->env, $context["error"], "html", null, true);
            echo "

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "                    <hr/>

                    <form data-request=\"onSend\">

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>NOME COMPLETO</label>
                                <input type=\"text\" class=\"form-control\" name=\"nome\">
                                <span style=\"color: #ff0000;\">";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "first", array(0 => "nome"), "method"), "html", null, true);
        echo "</span>
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-6\">
                                <label>E-MAIL</label>
                                <input type=\"text\" class=\"form-control\" name=\"email\">
                                <span style=\"color: #ff0000;\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "first", array(0 => "email"), "method"), "html", null, true);
        echo "</span>
                            </div>
                            <div class=\"col-md-6\">
                                <label>TELEFONE</label>
                                <input type=\"text\" class=\"form-control\" name=\"telefone\">
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>ASSUNTO</label>
                                <input type=\"text\" class=\"form-control\" name=\"assunto\">
                                <span style=\"color: #ff0000;\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "first", array(0 => "assunto"), "method"), "html", null, true);
        echo "</span>
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>MENSAGEM</label>
                                <textarea class=\"form-control\" style=\"min-height: 100px;\" name=\"mensagem\"></textarea>
                                <span style=\"color: #ff0000;\">";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "first", array(0 => "mensagem"), "method"), "html", null, true);
        echo "</span>
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <button class=\"btn btn-default btn-block\" type=\"submit\">ENVIAR</button>
                            </div>
                        </div>

                    </form>


                </div>

                <div class=\"col-md-6\">

                    <h2 style=\"color: #28a0ae;\">POSS ENGENHARIA</h2>
                    <p style=\"font-style: italic;color: #bbb;padding-left: 20px;\">Uma grande satifação pode atendar</p>
                   
                    <p style=\"padding-top: 20px;padding-left: 20px;\">

                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                        Av. Pires Fernandes, Nº. 39,<br/>Sala 01, St. Aeroporto,<br/>Goiânia - GO - CEP: 74.070-030<br/>
                        [ <a target=\"_blank\" class=\"linkhoverwhite\" href=\"https://goo.gl/maps/sFpMGtY4xdG2\">Como Chegar</a> ]<br/><br/>

                        <i class=\"fa fa-phone\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                         +55 (62) 3223-0375<br/><br/>
                        
                        <i class=\"fa fa-envelope\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                        contato@poss.com.br

                    </p>

                </div>

            </div>

        </div>

    </div>

</section>";
    }

    public function getTemplateName()
    {
        return "/var/www/poss-engenharia/web/plugins/bruno/contato/components/contactform/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 57,  91 => 49,  76 => 37,  65 => 29,  55 => 21,  46 => 18,  43 => 17,  39 => 16,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"about-us\" class=\"page_contato\">

    <div class=\"container\">

        <div class=\"center wow fadeInDown\">
            <h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Entre em contato conosco</h2>
        </div>

        <div class=\"row wow fadeInDown\" style=\"padding: 40px;\">
            
            <div class=\"col-md-10  col-md-offset-2 formulario\">
            
                <div class=\"col-md-6\" style=\"padding-right: 80px;\">

                    <h2>Preencha o formulário abaixo</h2>
                    {% for error in errors.all %}
                        
                        {{ error }}

                    {% endfor %}
                    <hr/>

                    <form data-request=\"onSend\">

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>NOME COMPLETO</label>
                                <input type=\"text\" class=\"form-control\" name=\"nome\">
                                <span style=\"color: #ff0000;\">{{ errors.first('nome') }}</span>
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-6\">
                                <label>E-MAIL</label>
                                <input type=\"text\" class=\"form-control\" name=\"email\">
                                <span style=\"color: #ff0000;\">{{ errors.first('email') }}</span>
                            </div>
                            <div class=\"col-md-6\">
                                <label>TELEFONE</label>
                                <input type=\"text\" class=\"form-control\" name=\"telefone\">
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>ASSUNTO</label>
                                <input type=\"text\" class=\"form-control\" name=\"assunto\">
                                <span style=\"color: #ff0000;\">{{ errors.first('assunto') }}</span>
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>MENSAGEM</label>
                                <textarea class=\"form-control\" style=\"min-height: 100px;\" name=\"mensagem\"></textarea>
                                <span style=\"color: #ff0000;\">{{ errors.first('mensagem') }}</span>
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <button class=\"btn btn-default btn-block\" type=\"submit\">ENVIAR</button>
                            </div>
                        </div>

                    </form>


                </div>

                <div class=\"col-md-6\">

                    <h2 style=\"color: #28a0ae;\">POSS ENGENHARIA</h2>
                    <p style=\"font-style: italic;color: #bbb;padding-left: 20px;\">Uma grande satifação pode atendar</p>
                   
                    <p style=\"padding-top: 20px;padding-left: 20px;\">

                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                        Av. Pires Fernandes, Nº. 39,<br/>Sala 01, St. Aeroporto,<br/>Goiânia - GO - CEP: 74.070-030<br/>
                        [ <a target=\"_blank\" class=\"linkhoverwhite\" href=\"https://goo.gl/maps/sFpMGtY4xdG2\">Como Chegar</a> ]<br/><br/>

                        <i class=\"fa fa-phone\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                         +55 (62) 3223-0375<br/><br/>
                        
                        <i class=\"fa fa-envelope\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                        contato@poss.com.br

                    </p>

                </div>

            </div>

        </div>

    </div>

</section>", "/var/www/poss-engenharia/web/plugins/bruno/contato/components/contactform/default.htm", "");
    }
}
