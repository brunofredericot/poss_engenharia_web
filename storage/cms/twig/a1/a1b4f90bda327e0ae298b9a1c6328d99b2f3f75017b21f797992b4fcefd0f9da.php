<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/detalhe-portfolio.htm */
class __TwigTemplate_eae49d7c1c74fd60e0df12e20cdaf1969b24f029d81dc6b4fcd14691ff6f5211 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["record"] = $this->getAttribute((isset($context["builderDetails"]) ? $context["builderDetails"] : null), "record", array());
        // line 2
        $context["displayColumn"] = $this->getAttribute((isset($context["builderDetails"]) ? $context["builderDetails"] : null), "displayColumn", array());
        // line 3
        $context["notFoundMessage"] = $this->getAttribute((isset($context["builderDetails"]) ? $context["builderDetails"] : null), "notFoundMessage", array());
        // line 4
        echo "
";
        // line 5
        $context["records"] = $this->getAttribute((isset($context["projetos_list"]) ? $context["projetos_list"] : null), "records", array());
        // line 6
        $context["displayColumnProjetos"] = $this->getAttribute((isset($context["projetos_list"]) ? $context["projetos_list"] : null), "displayColumn", array());
        // line 7
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["projetos_list"]) ? $context["projetos_list"] : null), "noRecordsMessage", array());
        // line 8
        $context["detailsPage"] = $this->getAttribute((isset($context["projetos_list"]) ? $context["projetos_list"] : null), "detailsPage", array());
        // line 9
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["projetos_list"]) ? $context["projetos_list"] : null), "detailsKeyColumn", array());
        // line 10
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["projetos_list"]) ? $context["projetos_list"] : null), "detailsUrlParameter", array());
        // line 11
        echo "
<section id=\"about-us\" style=\"padding-top: 30px;padding-bottom: 0;\">

    <div class=\"container\">

\t\t    <div class=\"get-started center wow fadeInDown\" style=\"padding-top: 0px;\">
\t\t    \t\t    
                <h2>
                    ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "titulo", array()), "html", null, true);
        echo "<br/>
                    <small><strong>Cliente:</strong> ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "cliente", array()), "html", null, true);
        echo "</small>
                    </h2>
                <p class=\"lead\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["record"]) ? $context["record"] : null), "descricao", array()), "html", null, true);
        echo "</p>
                <div class=\"request\">
                    <h4><a href=\"javascript:void(null)\">Imagens do projeto</a></h4>
                </div>
            </div>
            
\t</div>
\t
\t<div class=\"container\">
\t    ";
        // line 31
        if ((isset($context["record"]) ? $context["record"] : null)) {
            // line 32
            echo "      
            <!-- about us slider -->
\t\t\t<div id=\"about-slider\">
\t\t\t\t<div id=\"carousel-slider\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t\t\t
\t\t\t\t\t<!-- Indicators -->
\t\t\t\t  \t<ol class=\"carousel-indicators visible-xs\">
\t\t\t\t  \t    ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["record"]) ? $context["record"] : null), "galeria", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["imagem"]) {
                // line 40
                echo "\t\t\t\t  \t        ";
                if (($this->getAttribute($context["loop"], "index0", array()) == 0)) {
                    $context["active"] = "active";
                } else {
                    $context["active"] = "";
                }
                // line 41
                echo "\t\t\t\t\t        <li data-target=\"#carousel-slider\" data-slide-to=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index0", array()), "html", null, true);
                echo "\" class=\"";
                echo twig_escape_filter($this->env, (isset($context["active"]) ? $context["active"] : null), "html", null, true);
                echo "\"></li>
\t\t\t\t\t    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagem'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "\t\t\t\t  \t</ol>
\t\t\t\t  \t<!-- Indicators -->

\t\t\t\t\t<div class=\"carousel-inner\">
\t\t\t\t\t ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["record"]) ? $context["record"] : null), "galeria", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["imagem"]) {
                // line 48
                echo "\t\t\t\t\t ";
                if (($this->getAttribute($context["loop"], "index0", array()) == 0)) {
                    $context["active"] = "active";
                } else {
                    $context["active"] = "";
                }
                // line 49
                echo "\t\t\t\t\t\t<div class=\"item ";
                echo twig_escape_filter($this->env, (isset($context["active"]) ? $context["active"] : null), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t<img src=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->getAttribute($context["imagem"], "thumb", array(0 => 1200, 1 => 480, 2 => array("mode" => "crop")), "method"), "html", null, true);
                echo "\" class=\"img-responsive\" alt=\"\"> 
\t\t\t\t\t   </div>
\t\t\t\t\t ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagem'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<a class=\"left carousel-control hidden-xs\" href=\"#carousel-slider\" data-slide=\"prev\">
\t\t\t\t\t\t<i class=\"fa fa-angle-left\"></i> 
\t\t\t\t\t</a>
\t\t\t\t\t
\t\t\t\t\t<a class=\" right carousel-control hidden-xs\"href=\"#carousel-slider\" data-slide=\"next\">
\t\t\t\t\t\t<i class=\"fa fa-angle-right\"></i> 
\t\t\t\t\t</a>
\t\t\t\t</div> <!--/#carousel-slider-->
\t\t\t</div><!--/#about-slider-->
            
            
        ";
        } else {
            // line 67
            echo "            ";
            echo twig_escape_filter($this->env, (isset($context["notFoundMessage"]) ? $context["notFoundMessage"] : null), "html", null, true);
            echo "
        ";
        }
        // line 69
        echo "\t</div>

</section><!--/about-us-->

 <section id=\"portfolio\" style=\"padding-bottom: 0;\">
        <div class=\"container-fluid\">

            <div class=\"center\">
               <h2>Portfolio</h2>
               <p class=\"lead\">Conheça abaixo alguns dos nossos principais projetos realizados</p>
            </div>
        
            <ul class=\"portfolio-filter text-center\">
                <li><a class=\"btn btn-default active\" href=\"#\" data-filter=\"*\">TODOS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".projetos\">PROJETOS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_comeciais\">OBRAS COMERCIAIS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_industriais\">OBRAS INDUSTRIAS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_residenciais\">OBRAS RESIDENCIAIS</a></li>
            </ul><!--/#portfolio-filter-->

            <div class=\"row\">

                <div class=\"portfolio-items\">

                ";
        // line 93
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["projeto"]) {
            // line 94
            echo "
                    <div class=\"portfolio-item ";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute($context["projeto"], "categoria", array()), "html", null, true);
            echo " col-xs-12 col-sm-4 col-md-3\">
                        <div class=\"recent-work-wrap\">
                            <img class=\"img-responsive\" src=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["projeto"], "foto1", array()), "thumb", array(0 => 327, 1 => 248, 2 => array("mode" => "crop")), "method"), "html", null, true);
            echo "\" alt=\"\">
                            <div class=\"overlay\">
                                <div class=\"recent-work-inner\">

                                    <h3><a href=\"#\">";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["projeto"], "titulo", array()), "html", null, true);
            echo "</a> </h3>
                                    
                                    <p>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["projeto"], "descricao", array()), "html", null, true);
            echo "</p>
                                    
                                    <a class=\"preview\" href=\"";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["projeto"], "foto1", array()), "path", array()), "html", null, true);
            echo "\" rel=\"prettyPhoto\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["projeto"], "titulo", array()), "html", null, true);
            echo "\">
                                    <i class=\"fa fa-eye\"></i> Ampliar</a><br/>
                                    
                                    ";
            // line 108
            if ((isset($context["detailsPage"]) ? $context["detailsPage"] : null)) {
                // line 109
                echo "                                        <a class=\"preview\" href=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter((isset($context["detailsPage"]) ? $context["detailsPage"] : null), array((isset($context["detailsUrlParameter"]) ? $context["detailsUrlParameter"] : null) => $this->getAttribute($context["projeto"], (isset($context["detailsKeyColumn"]) ? $context["detailsKeyColumn"] : null))));
                echo "\" 
                                        >
                                        <i class=\"fa fa-eye\"></i> Detalhes</a>
                                    ";
            }
            // line 113
            echo "            
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['projeto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 119
        echo " 

                </div>
                
            </div>
        </div>
</section><!--/#portfolio-item-->";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/detalhe-portfolio.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 119,  272 => 113,  264 => 109,  262 => 108,  254 => 105,  249 => 103,  244 => 101,  237 => 97,  232 => 95,  229 => 94,  225 => 93,  199 => 69,  193 => 67,  177 => 53,  160 => 50,  155 => 49,  148 => 48,  131 => 47,  125 => 43,  106 => 41,  99 => 40,  82 => 39,  73 => 32,  71 => 31,  59 => 22,  54 => 20,  50 => 19,  40 => 11,  38 => 10,  36 => 9,  34 => 8,  32 => 7,  30 => 6,  28 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set record = builderDetails.record %}
{% set displayColumn = builderDetails.displayColumn %}
{% set notFoundMessage = builderDetails.notFoundMessage %}

{% set records = projetos_list.records %}
{% set displayColumnProjetos = projetos_list.displayColumn %}
{% set noRecordsMessage = projetos_list.noRecordsMessage %}
{% set detailsPage = projetos_list.detailsPage %}
{% set detailsKeyColumn = projetos_list.detailsKeyColumn %}
{% set detailsUrlParameter = projetos_list.detailsUrlParameter %}

<section id=\"about-us\" style=\"padding-top: 30px;padding-bottom: 0;\">

    <div class=\"container\">

\t\t    <div class=\"get-started center wow fadeInDown\" style=\"padding-top: 0px;\">
\t\t    \t\t    
                <h2>
                    {{ record.titulo }}<br/>
                    <small><strong>Cliente:</strong> {{ record.cliente }}</small>
                    </h2>
                <p class=\"lead\">{{ record.descricao }}</p>
                <div class=\"request\">
                    <h4><a href=\"javascript:void(null)\">Imagens do projeto</a></h4>
                </div>
            </div>
            
\t</div>
\t
\t<div class=\"container\">
\t    {% if record %}
      
            <!-- about us slider -->
\t\t\t<div id=\"about-slider\">
\t\t\t\t<div id=\"carousel-slider\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t\t\t
\t\t\t\t\t<!-- Indicators -->
\t\t\t\t  \t<ol class=\"carousel-indicators visible-xs\">
\t\t\t\t  \t    {% for imagem in record.galeria %}
\t\t\t\t  \t        {% if loop.index0 == 0 %}{% set active='active' %}{% else %}{% set active='' %}{%endif%}
\t\t\t\t\t        <li data-target=\"#carousel-slider\" data-slide-to=\"{{ loop.index0 }}\" class=\"{{ active }}\"></li>
\t\t\t\t\t    {% endfor %}
\t\t\t\t  \t</ol>
\t\t\t\t  \t<!-- Indicators -->

\t\t\t\t\t<div class=\"carousel-inner\">
\t\t\t\t\t {% for imagem in record.galeria %}
\t\t\t\t\t {% if loop.index0 == 0 %}{% set active='active' %}{% else %}{% set active='' %}{%endif%}
\t\t\t\t\t\t<div class=\"item {{ active }}\">
\t\t\t\t\t\t\t<img src=\"{{ imagem.thumb(1200,480,{'mode':'crop'}) }}\" class=\"img-responsive\" alt=\"\"> 
\t\t\t\t\t   </div>
\t\t\t\t\t {% endfor %}
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<a class=\"left carousel-control hidden-xs\" href=\"#carousel-slider\" data-slide=\"prev\">
\t\t\t\t\t\t<i class=\"fa fa-angle-left\"></i> 
\t\t\t\t\t</a>
\t\t\t\t\t
\t\t\t\t\t<a class=\" right carousel-control hidden-xs\"href=\"#carousel-slider\" data-slide=\"next\">
\t\t\t\t\t\t<i class=\"fa fa-angle-right\"></i> 
\t\t\t\t\t</a>
\t\t\t\t</div> <!--/#carousel-slider-->
\t\t\t</div><!--/#about-slider-->
            
            
        {% else %}
            {{ notFoundMessage }}
        {% endif %}
\t</div>

</section><!--/about-us-->

 <section id=\"portfolio\" style=\"padding-bottom: 0;\">
        <div class=\"container-fluid\">

            <div class=\"center\">
               <h2>Portfolio</h2>
               <p class=\"lead\">Conheça abaixo alguns dos nossos principais projetos realizados</p>
            </div>
        
            <ul class=\"portfolio-filter text-center\">
                <li><a class=\"btn btn-default active\" href=\"#\" data-filter=\"*\">TODOS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".projetos\">PROJETOS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_comeciais\">OBRAS COMERCIAIS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_industriais\">OBRAS INDUSTRIAS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_residenciais\">OBRAS RESIDENCIAIS</a></li>
            </ul><!--/#portfolio-filter-->

            <div class=\"row\">

                <div class=\"portfolio-items\">

                {% for projeto in records %}

                    <div class=\"portfolio-item {{ projeto.categoria }} col-xs-12 col-sm-4 col-md-3\">
                        <div class=\"recent-work-wrap\">
                            <img class=\"img-responsive\" src=\"{{ projeto.foto1.thumb(327,248,{'mode':'crop'}) }}\" alt=\"\">
                            <div class=\"overlay\">
                                <div class=\"recent-work-inner\">

                                    <h3><a href=\"#\">{{ projeto.titulo }}</a> </h3>
                                    
                                    <p>{{ projeto.descricao }}</p>
                                    
                                    <a class=\"preview\" href=\"{{ projeto.foto1.path }}\" rel=\"prettyPhoto\" title=\"{{ projeto.titulo }}\">
                                    <i class=\"fa fa-eye\"></i> Ampliar</a><br/>
                                    
                                    {% if detailsPage %}
                                        <a class=\"preview\" href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(projeto, detailsKeyColumn) }) }}\" 
                                        >
                                        <i class=\"fa fa-eye\"></i> Detalhes</a>
                                    {% endif %}
            
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                {% endfor %} 

                </div>
                
            </div>
        </div>
</section><!--/#portfolio-item-->", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/detalhe-portfolio.htm", "");
    }
}
