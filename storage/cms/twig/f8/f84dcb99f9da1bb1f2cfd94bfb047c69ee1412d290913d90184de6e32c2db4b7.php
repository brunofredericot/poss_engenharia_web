<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/samples/empresa.htm */
class __TwigTemplate_79fba36314024b195efab6f75dc596b6157b4fae335a93f996afb265ff13b539 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"about-us\" class=\"pagina_empresa\">

        <div class=\"container\">
\t\t\t
\t\t\t<div class=\"center wow fadeInDown\">
\t\t\t\t<h2><img src=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Empresa</h2>
\t\t\t\t<p class=\"lead\">
\t\t\t\tSomos uma empresa dedicada a projetos e obras com soluções sustentáveis,<br/> que abrange todo Brasil, buscando sempre excelencia em nosso trabalho.
\t\t\t\t</p>
\t\t\t</div>\t
\t\t</div>

\t\t\t<div class=\"wow fadeInDown\" style=\"padding: 40px;background: #f2f2f2;font-size: 1.1em; line-height: 2em;\">

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h2 class=\"title-blue\">Nossa Missão</h2>
\t\t\t\t\t\t<p>
\t\t\t\t\t\t\tEstar entre as empresas líderes no mercado em Engenharia e Construção Cívil do país, baseada em qualidade, cumprimento de prazos e sustentabilidade.
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h2 class=\"title-blue\">Nossa Visão</h2>
\t\t\t\t\t\t<p>
 Ser reconhecida como empresa grande, sólida e ágil, atuando em toda a cadeia de fornecimento de serviços, suprindo as demandas do mercado, de forma eficaz e eficiente, comprometida com a
 qualidade e a inovação.
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h2 class=\"title-blue\">Nossos valores</h2>
\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\tÉtica e Honestidade;
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\tSustentabilidade – identificar as melhores soluções sustentáveis para projetos e serviços na contrução civil;
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>Compromisso com o cliente;</li>
\t\t\t\t\t\t\t\t<li>Flexibilidade – identificar as necessidades do cliente, utilizando nossas expertises sempre da forma mais adequada e efetiva;</li>
\t\t\t\t\t\t\t\t<li>Qualidade dos trabalhos – a excelência por meio da união de competências;</li>
\t\t\t\t\t\t\t\t<li>Disposição para os Desafios e Proatividade.</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>

\t\t\t<div class=\"center wow fadeInDown\" style=\"padding-top: 60px;\">
\t\t\t\t<h2><img src=\"";
        // line 51
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Nossa Equipe</h2>
\t\t\t\t<p class=\"lead\">
\t\t\t\t\tCom uma equipe de bastante conhecimento técnico buscamos qualidade, Agilidade e confiabilidade dos serviços prestados.
\t\t\t\t</p>
\t\t\t</div>\t

\t\t\t<div class=\"row\">
                <div class=\"col-md-4 wow fadeInDown\">
                    <div class=\"clients-comments text-center\">
                        <img src=\"";
        // line 60
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/client1.png");
        echo "\" class=\"img-circle\" alt=\"\">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
                        <h4><span>-John Doe /</span>  Director of corlate.com</h4>
                    </div>
                </div>
                <div class=\"col-md-4 wow fadeInDown\">
                    <div class=\"clients-comments text-center\">
                        <img src=\"";
        // line 67
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/client2.png");
        echo "\" class=\"img-circle\" alt=\"\">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
                        <h4><span>-John Doe /</span>  Director of corlate.com</h4>
                    </div>
                </div>
                <div class=\"col-md-4 wow fadeInDown\">
                    <div class=\"clients-comments text-center\">
                        <img src=\"";
        // line 74
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/client3.png");
        echo "\" class=\"img-circle\" alt=\"\">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
                        <h4><span>-John Doe /</span>  Director of corlate.com</h4>
                    </div>
                </div>
           </div>

\t\t</div><!--/.container-->
    </section><!--/about-us-->";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/samples/empresa.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 74,  96 => 67,  86 => 60,  74 => 51,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"about-us\" class=\"pagina_empresa\">

        <div class=\"container\">
\t\t\t
\t\t\t<div class=\"center wow fadeInDown\">
\t\t\t\t<h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Empresa</h2>
\t\t\t\t<p class=\"lead\">
\t\t\t\tSomos uma empresa dedicada a projetos e obras com soluções sustentáveis,<br/> que abrange todo Brasil, buscando sempre excelencia em nosso trabalho.
\t\t\t\t</p>
\t\t\t</div>\t
\t\t</div>

\t\t\t<div class=\"wow fadeInDown\" style=\"padding: 40px;background: #f2f2f2;font-size: 1.1em; line-height: 2em;\">

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h2 class=\"title-blue\">Nossa Missão</h2>
\t\t\t\t\t\t<p>
\t\t\t\t\t\t\tEstar entre as empresas líderes no mercado em Engenharia e Construção Cívil do país, baseada em qualidade, cumprimento de prazos e sustentabilidade.
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h2 class=\"title-blue\">Nossa Visão</h2>
\t\t\t\t\t\t<p>
 Ser reconhecida como empresa grande, sólida e ágil, atuando em toda a cadeia de fornecimento de serviços, suprindo as demandas do mercado, de forma eficaz e eficiente, comprometida com a
 qualidade e a inovação.
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<h2 class=\"title-blue\">Nossos valores</h2>
\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\tÉtica e Honestidade;
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\tSustentabilidade – identificar as melhores soluções sustentáveis para projetos e serviços na contrução civil;
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>Compromisso com o cliente;</li>
\t\t\t\t\t\t\t\t<li>Flexibilidade – identificar as necessidades do cliente, utilizando nossas expertises sempre da forma mais adequada e efetiva;</li>
\t\t\t\t\t\t\t\t<li>Qualidade dos trabalhos – a excelência por meio da união de competências;</li>
\t\t\t\t\t\t\t\t<li>Disposição para os Desafios e Proatividade.</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>

\t\t\t<div class=\"center wow fadeInDown\" style=\"padding-top: 60px;\">
\t\t\t\t<h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Nossa Equipe</h2>
\t\t\t\t<p class=\"lead\">
\t\t\t\t\tCom uma equipe de bastante conhecimento técnico buscamos qualidade, Agilidade e confiabilidade dos serviços prestados.
\t\t\t\t</p>
\t\t\t</div>\t

\t\t\t<div class=\"row\">
                <div class=\"col-md-4 wow fadeInDown\">
                    <div class=\"clients-comments text-center\">
                        <img src=\"{{ 'assets/images/client1.png'|theme }}\" class=\"img-circle\" alt=\"\">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
                        <h4><span>-John Doe /</span>  Director of corlate.com</h4>
                    </div>
                </div>
                <div class=\"col-md-4 wow fadeInDown\">
                    <div class=\"clients-comments text-center\">
                        <img src=\"{{ 'assets/images/client2.png'|theme }}\" class=\"img-circle\" alt=\"\">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
                        <h4><span>-John Doe /</span>  Director of corlate.com</h4>
                    </div>
                </div>
                <div class=\"col-md-4 wow fadeInDown\">
                    <div class=\"clients-comments text-center\">
                        <img src=\"{{ 'assets/images/client3.png'|theme }}\" class=\"img-circle\" alt=\"\">
                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
                        <h4><span>-John Doe /</span>  Director of corlate.com</h4>
                    </div>
                </div>
           </div>

\t\t</div><!--/.container-->
    </section><!--/about-us-->", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/samples/empresa.htm", "");
    }
}
