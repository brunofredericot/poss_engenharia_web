<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/samples/portfolio.htm */
class __TwigTemplate_42f9579e358189006d553740edc540d90354f1bddd6ab93e12885b7415295d62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"portfolio\" style=\"padding-bottom: 0;\">
        <div class=\"container-fluid\">

            <div class=\"center\" style=\"padding-bottom: 0;\">
               <h2><img src=\"";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Portfolio</h2>
               <p class=\"lead\">Conheça abaixo alguns dos nossos principais projetos realizados</p>
            </div>
        
            <ul class=\"portfolio-filter text-center\">
                <li><a class=\"btn btn-default active\" href=\"#\" data-filter=\"*\">TODOS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".instaladora\">INSTALADORA</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".projetos\">PROJETOS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_comeciais\">OBRAS COMERCIAIS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_industriais\">OBRAS INDUSTRIAS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".consultoria\">CONSULTORIA</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_residenciais\">OBRAS RESIDENCIAIS</a></li>
            </ul><!--/#portfolio-filter-->

            <div class=\"row\">

                <div class=\"portfolio-items\">

                ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["projeto"]) {
            // line 24
            echo "
                    <div class=\"portfolio-item ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["projeto"], "categoria", array()), "html", null, true);
            echo " col-xs-12 col-sm-4 col-md-3\">
                        <div class=\"recent-work-wrap\">
                            <img class=\"img-responsive\" src=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["projeto"], "foto1", array()), "thumb", array(0 => 327, 1 => 248, 2 => array("mode" => "crop")), "method"), "html", null, true);
            echo "\" alt=\"\">
                            <div class=\"overlay\">
                                <div class=\"recent-work-inner\">

                                    <h3><a href=\"#\">";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["projeto"], "titulo", array()), "html", null, true);
            echo "</a> </h3>
                                    
                                    <p>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["projeto"], "descricao", array()), "html", null, true);
            echo "</p>
                                    
                                    <a class=\"preview\" href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["projeto"], "foto1", array()), "path", array()), "html", null, true);
            echo "\" rel=\"prettyPhoto\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["projeto"], "titulo", array()), "html", null, true);
            echo "\">
                                    <i class=\"fa fa-eye\"></i> Ampliar</a><br/>
                                    
                                    ";
            // line 38
            if ((isset($context["detailsPage"]) ? $context["detailsPage"] : null)) {
                // line 39
                echo "                                        <a class=\"preview\" href=\"";
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter((isset($context["detailsPage"]) ? $context["detailsPage"] : null), array((isset($context["detailsUrlParameter"]) ? $context["detailsUrlParameter"] : null) => $this->getAttribute($context["projeto"], (isset($context["detailsKeyColumn"]) ? $context["detailsKeyColumn"] : null))));
                echo "\" 
                                        >
                                        <i class=\"fa fa-eye\"></i> Detalhes</a>
                                    ";
            }
            // line 43
            echo "            
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['projeto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo " 

                </div>
                
            </div>
        </div>
</section><!--/#portfolio-item-->";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/samples/portfolio.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 49,  93 => 43,  85 => 39,  83 => 38,  75 => 35,  70 => 33,  65 => 31,  58 => 27,  53 => 25,  50 => 24,  46 => 23,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"portfolio\" style=\"padding-bottom: 0;\">
        <div class=\"container-fluid\">

            <div class=\"center\" style=\"padding-bottom: 0;\">
               <h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Portfolio</h2>
               <p class=\"lead\">Conheça abaixo alguns dos nossos principais projetos realizados</p>
            </div>
        
            <ul class=\"portfolio-filter text-center\">
                <li><a class=\"btn btn-default active\" href=\"#\" data-filter=\"*\">TODOS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".instaladora\">INSTALADORA</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".projetos\">PROJETOS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_comeciais\">OBRAS COMERCIAIS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_industriais\">OBRAS INDUSTRIAS</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".consultoria\">CONSULTORIA</a></li>
                <li><a class=\"btn btn-default\" href=\"#\" data-filter=\".obras_residenciais\">OBRAS RESIDENCIAIS</a></li>
            </ul><!--/#portfolio-filter-->

            <div class=\"row\">

                <div class=\"portfolio-items\">

                {% for projeto in records %}

                    <div class=\"portfolio-item {{ projeto.categoria }} col-xs-12 col-sm-4 col-md-3\">
                        <div class=\"recent-work-wrap\">
                            <img class=\"img-responsive\" src=\"{{ projeto.foto1.thumb(327,248,{'mode':'crop'}) }}\" alt=\"\">
                            <div class=\"overlay\">
                                <div class=\"recent-work-inner\">

                                    <h3><a href=\"#\">{{ projeto.titulo }}</a> </h3>
                                    
                                    <p>{{ projeto.descricao }}</p>
                                    
                                    <a class=\"preview\" href=\"{{ projeto.foto1.path }}\" rel=\"prettyPhoto\" title=\"{{ projeto.titulo }}\">
                                    <i class=\"fa fa-eye\"></i> Ampliar</a><br/>
                                    
                                    {% if detailsPage %}
                                        <a class=\"preview\" href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(projeto, detailsKeyColumn) }) }}\" 
                                        >
                                        <i class=\"fa fa-eye\"></i> Detalhes</a>
                                    {% endif %}
            
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                {% endfor %} 

                </div>
                
            </div>
        </div>
</section><!--/#portfolio-item-->", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/samples/portfolio.htm", "");
    }
}
