<?php

/* /var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/footer.htm */
class __TwigTemplate_261e725311e053d281dca62321ccaed78fd81b7669df49c89da77c36cb3e3d34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-12\" style=\"text-align: center;\">
                <ul class=\"social-icons\">
                    <li><a href=\"https://www.facebook.com/possengenharia/\" target=\"_blank\"><i class=\"fa fa-facebook-square\" aria-hidden=\"true\"></i></a></li>
                    <li><a href=\"\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a></li>
                </ul>
            </div>
            <div class=\"col-sm-12\" style=\"text-align: center;\">
                <p style=\"text-align: center;padding-top: 0px;padding-bottom:0;\">&copy;  ";
        // line 10
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " Poss Engenharia Ltda. Todos os direitos reservados</p>
            </div>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "/var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 10,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-12\" style=\"text-align: center;\">
                <ul class=\"social-icons\">
                    <li><a href=\"https://www.facebook.com/possengenharia/\" target=\"_blank\"><i class=\"fa fa-facebook-square\" aria-hidden=\"true\"></i></a></li>
                    <li><a href=\"\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a></li>
                </ul>
            </div>
            <div class=\"col-sm-12\" style=\"text-align: center;\">
                <p style=\"text-align: center;padding-top: 0px;padding-bottom:0;\">&copy;  {{ \"now\"|date(\"Y\") }} Poss Engenharia Ltda. Todos os direitos reservados</p>
            </div>
        </div>
    </div>", "/var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/footer.htm", "");
    }
}
