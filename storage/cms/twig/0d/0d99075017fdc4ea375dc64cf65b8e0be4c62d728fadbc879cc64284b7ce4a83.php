<?php

/* /var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/carousel.htm */
class __TwigTemplate_685a4da9be5425aa7b4af0a26a7646b4a2054933a99ebcc7e4e9f1562b3c133f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"carousel slide\">
            <ol class=\"carousel-indicators\">
                <li data-target=\"#main-slider\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#main-slider\" data-slide-to=\"1\"></li>
                <li data-target=\"#main-slider\" data-slide-to=\"2\"></li>
            </ol>
            <div class=\"carousel-inner\">

                <div class=\"item active\" style=\"background-image: url(";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/slider/banner2.jpg");
        echo ")\">
                    <div class=\"container\">
                        <div class=\"row slide-margin\">

                            <div class=\"col-sm-8 col-sm-offset-2\" style=\"text-align: center;\">
                                <div class=\"carousel-content\">
                                    <h2 class=\"animation animated-item-1 title-italic-banner\">
                                    Temos as Melhores
                                    </h2>
                                    <h1 class=\"animation animated-item-2 title-strong-banner\">
                                    Soluções para sua obra
                                    </h1>
                                    <a class=\"btn-slide animation animated-item-3\" href=\"/portfolio\">Saiba Mais</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--/.item-->


                <div class=\"item\" style=\"background-image: url(";
        // line 30
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/slider/banner3.jpg");
        echo ")\">
                    <div class=\"container\">
                        <div class=\"row slide-margin\">
                            <div class=\"col-sm-6\">
                                <div class=\"carousel-content\">
                                    <h1 class=\"animation animated-item-1\">Especialidade em obras comerciais</h1>
                                    <h2 class=\"animation animated-item-2\">Conheça mais sobre nosso portolio</h2>
                                    <a class=\"btn-slide animation animated-item-3\" href=\"/portfolio\">Saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->

                <div class=\"item\" style=\"background-image: url(";
        // line 44
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/slider/banner_home.jpg");
        echo ")\">
                    <div class=\"container\">
                        <div class=\"row slide-margin\">
                            <div class=\"col-sm-6\">
                                <div class=\"carousel-content\">
                                    <h1 class=\"animation animated-item-1\">Experiência no mercado há vários anos</h1>
                                    <h2 class=\"animation animated-item-2\">Qualidade garantida dos nosos trabalhos e  excelência por meio da união de competências</h2>
                                    <a class=\"btn-slide animation animated-item-3\" href=\"/empresa\">Saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->";
    }

    public function getTemplateName()
    {
        return "/var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/carousel.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 44,  53 => 30,  29 => 9,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"carousel slide\">
            <ol class=\"carousel-indicators\">
                <li data-target=\"#main-slider\" data-slide-to=\"0\" class=\"active\"></li>
                <li data-target=\"#main-slider\" data-slide-to=\"1\"></li>
                <li data-target=\"#main-slider\" data-slide-to=\"2\"></li>
            </ol>
            <div class=\"carousel-inner\">

                <div class=\"item active\" style=\"background-image: url({{ 'assets/images/slider/banner2.jpg'|theme }})\">
                    <div class=\"container\">
                        <div class=\"row slide-margin\">

                            <div class=\"col-sm-8 col-sm-offset-2\" style=\"text-align: center;\">
                                <div class=\"carousel-content\">
                                    <h2 class=\"animation animated-item-1 title-italic-banner\">
                                    Temos as Melhores
                                    </h2>
                                    <h1 class=\"animation animated-item-2 title-strong-banner\">
                                    Soluções para sua obra
                                    </h1>
                                    <a class=\"btn-slide animation animated-item-3\" href=\"/portfolio\">Saiba Mais</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--/.item-->


                <div class=\"item\" style=\"background-image: url({{ 'assets/images/slider/banner3.jpg'|theme }})\">
                    <div class=\"container\">
                        <div class=\"row slide-margin\">
                            <div class=\"col-sm-6\">
                                <div class=\"carousel-content\">
                                    <h1 class=\"animation animated-item-1\">Especialidade em obras comerciais</h1>
                                    <h2 class=\"animation animated-item-2\">Conheça mais sobre nosso portolio</h2>
                                    <a class=\"btn-slide animation animated-item-3\" href=\"/portfolio\">Saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->

                <div class=\"item\" style=\"background-image: url({{ 'assets/images/slider/banner_home.jpg'|theme }})\">
                    <div class=\"container\">
                        <div class=\"row slide-margin\">
                            <div class=\"col-sm-6\">
                                <div class=\"carousel-content\">
                                    <h1 class=\"animation animated-item-1\">Experiência no mercado há vários anos</h1>
                                    <h2 class=\"animation animated-item-2\">Qualidade garantida dos nosos trabalhos e  excelência por meio da união de competências</h2>
                                    <a class=\"btn-slide animation animated-item-3\" href=\"/empresa\">Saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->", "/var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/carousel.htm", "");
    }
}
