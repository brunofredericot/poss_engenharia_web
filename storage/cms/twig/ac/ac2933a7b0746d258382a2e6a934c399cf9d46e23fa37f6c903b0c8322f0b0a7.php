<?php

/* /var/www/poss-engenharia/web/themes/jtherczeg-corlate/pages/samples/clientes.htm */
class __TwigTemplate_27061c2d3a7b6a1c8cef727d3bcd517f4ac650251170f97de2d5e96347c93422 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["records"] = $this->getAttribute((isset($context["clientes_list"]) ? $context["clientes_list"] : null), "records", array());
        // line 2
        $context["displayColumn"] = $this->getAttribute((isset($context["clientes_list"]) ? $context["clientes_list"] : null), "displayColumn", array());
        // line 3
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["clientes_list"]) ? $context["clientes_list"] : null), "noRecordsMessage", array());
        // line 4
        $context["detailsPage"] = $this->getAttribute((isset($context["clientes_list"]) ? $context["clientes_list"] : null), "detailsPage", array());
        // line 5
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["clientes_list"]) ? $context["clientes_list"] : null), "detailsKeyColumn", array());
        // line 6
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["clientes_list"]) ? $context["clientes_list"] : null), "detailsUrlParameter", array());
        // line 7
        echo "
<section id=\"about-us\" class=\"pagina_clientes\">

    <div class=\"container\">
\t\t\t
\t\t<div class=\"center wow fadeInDown\">
\t\t\t<h2><img src=\"";
        // line 13
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Nossos Clientes</h2>\t\t\t
\t\t</div>\t

\t\t<div class=\"row wow fadeInDown\" style=\"padding: 40px;\">
\t\t\t
\t    ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 19
            echo "            ";
            ob_start();
            echo "            
               <div class=\"col-md-2 item-logo-cliente\">
               <span class=\"helper\"></span> <img src=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["record"], "logo", array()), "path", array()), "html", null, true);
            echo "\"/>
               </div>                
            ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 24
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "
\t\t</div>

\t</div><!--/.container-->

</section><!--/about-us-->";
    }

    public function getTemplateName()
    {
        return "/var/www/poss-engenharia/web/themes/jtherczeg-corlate/pages/samples/clientes.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 25,  63 => 24,  57 => 21,  51 => 19,  47 => 18,  39 => 13,  31 => 7,  29 => 6,  27 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set records = clientes_list.records %}
{% set displayColumn = clientes_list.displayColumn %}
{% set noRecordsMessage = clientes_list.noRecordsMessage %}
{% set detailsPage = clientes_list.detailsPage %}
{% set detailsKeyColumn = clientes_list.detailsKeyColumn %}
{% set detailsUrlParameter = clientes_list.detailsUrlParameter %}

<section id=\"about-us\" class=\"pagina_clientes\">

    <div class=\"container\">
\t\t\t
\t\t<div class=\"center wow fadeInDown\">
\t\t\t<h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Nossos Clientes</h2>\t\t\t
\t\t</div>\t

\t\t<div class=\"row wow fadeInDown\" style=\"padding: 40px;\">
\t\t\t
\t    {% for record in records %}
            {% spaceless %}            
               <div class=\"col-md-2 item-logo-cliente\">
               <span class=\"helper\"></span> <img src=\"{{ record.logo.path }}\"/>
               </div>                
            {% endspaceless %}
        {% endfor %}

\t\t</div>

\t</div><!--/.container-->

</section><!--/about-us-->", "/var/www/poss-engenharia/web/themes/jtherczeg-corlate/pages/samples/clientes.htm", "");
    }
}
