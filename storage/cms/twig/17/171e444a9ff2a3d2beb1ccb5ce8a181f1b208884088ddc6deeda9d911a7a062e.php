<?php

/* /var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/home/servicos.htm */
class __TwigTemplate_6226225068c4abed6768fccace232f7bc530faf34bfe27310b60070f4b9c5c83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
          
           <div class=\"center wow fadeInDown\" style=\"padding-bottom:0;\">
                <h2><img src=\"";
        // line 4
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Nossos Serviços</h2>
            </div>

            <div class=\"row\">
                <div class=\"features list-servicos-home\">

                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/instaladora\"><div><img src=\"";
        // line 11
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/custom-icon/engineering.png");
        echo "\"/></div>
                        <div>
                            <h4>INSTALADORA</h4>
                        </div>
                        </a>
                    </div>
                    
                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/portfolio\"><div><i class=\"ti-Line-Crop\"></i></div>
                        <div>
                            <h4>PROJETOS</h4>
                        </div>
                        </a>
                    </div>

                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/portfolio\"><div><i class=\"ti-Line-Store\"></i></div>
                        <div>
                            <h4>OBRAS COMERCIAIS</h4>
                        </div>
                    </div>

                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                       <a href=\"/portfolio\"> <div><img src=\"";
        // line 34
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/custom-icon/003-pollution_.png");
        echo "\"/></div>
                        <div>
                            <h4>OBRAS INDUSTRIAIS</h4>
                        </div>
                        </a>
                    </div>
                    
                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/portfolio\"><div><img src=\"";
        // line 42
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/custom-icon/001-teamwork.png");
        echo "\"/></div>
                        <div>
                            <h4>CONSULTORIA</h4>
                        </div>
                        </a>
                    </div>

                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/portfolio\"><div><i class=\"ti-Line-Home\"></i></div>
                        <div>
                            <h4>OBRAS RESIDENCIAIS</h4>
                        </div>
                        </a>
                    </div>

                </div><!--/.services-->
            </div><!--/.row-->    
        </div><!--/.container-->";
    }

    public function getTemplateName()
    {
        return "/var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/home/servicos.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 42,  60 => 34,  34 => 11,  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">
          
           <div class=\"center wow fadeInDown\" style=\"padding-bottom:0;\">
                <h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Nossos Serviços</h2>
            </div>

            <div class=\"row\">
                <div class=\"features list-servicos-home\">

                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/instaladora\"><div><img src=\"{{ 'assets/css/custom-icon/engineering.png'|theme }}\"/></div>
                        <div>
                            <h4>INSTALADORA</h4>
                        </div>
                        </a>
                    </div>
                    
                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/portfolio\"><div><i class=\"ti-Line-Crop\"></i></div>
                        <div>
                            <h4>PROJETOS</h4>
                        </div>
                        </a>
                    </div>

                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/portfolio\"><div><i class=\"ti-Line-Store\"></i></div>
                        <div>
                            <h4>OBRAS COMERCIAIS</h4>
                        </div>
                    </div>

                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                       <a href=\"/portfolio\"> <div><img src=\"{{ 'assets/css/custom-icon/003-pollution_.png'|theme }}\"/></div>
                        <div>
                            <h4>OBRAS INDUSTRIAIS</h4>
                        </div>
                        </a>
                    </div>
                    
                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/portfolio\"><div><img src=\"{{ 'assets/css/custom-icon/001-teamwork.png'|theme }}\"/></div>
                        <div>
                            <h4>CONSULTORIA</h4>
                        </div>
                        </a>
                    </div>

                    <div class=\"col-md-2 col-sm-6 wow fadeInDown list-servicos-home-item\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
                        <a href=\"/portfolio\"><div><i class=\"ti-Line-Home\"></i></div>
                        <div>
                            <h4>OBRAS RESIDENCIAIS</h4>
                        </div>
                        </a>
                    </div>

                </div><!--/.services-->
            </div><!--/.row-->    
        </div><!--/.container-->", "/var/www/poss-engenharia/web/themes/jtherczeg-corlate/partials/home/servicos.htm", "");
    }
}
