<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/nav.htm */
class __TwigTemplate_f60aae174d33998da92839ad8c95f91cfd47ca5416a9bf86d84b96b30a152345 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["links"] = array("" => array(0 => "home", 1 => "HOME"), "empresa" => array(0 => "samples/empresa", 1 => "EMPRESA"), "portfolio" => array(0 => "samples/portfolio", 1 => "PORTFOLIO"), "clientes" => array(0 => "samples/clientes", 1 => "CLIENTES"), "trabalhe-conosco" => array(0 => "trabalhe_conosco", 1 => "TRABALHE CONOSCO"), "contato" => array(0 => "samples/contato", 1 => "CONTATO"));
        // line 12
        echo "
";
        // line 26
        echo "
";
        // line 27
        $context["nav"] = $this;
        // line 28
        echo "

<nav class=\"navbar navbar-inverse\" role=\"banner\">
\t<div class=\"container\">
\t\t<div class=\"navbar-header\">
\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t<span class=\"sr-only\">Toggle navigation</span>
\t\t\t<span class=\"icon-bar\"></span>
\t\t\t<span class=\"icon-bar\"></span>
\t\t\t<span class=\"icon-bar\"></span>
\t\t\t</button>
\t\t\t<a class=\"navbar-brand\" href=\"";
        // line 39
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("home");
        echo "\"><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/logo2.png");
        echo "\" alt=\"logo\"></a>
\t\t</div>
\t\t<div class=\"collapse navbar-collapse navbar-right\">
\t\t\t
\t\t\t<ul class=\"nav navbar-nav\">
\t\t\t\t";
        // line 44
        echo $context["nav"]->getrender_menu((isset($context["links"]) ? $context["links"] : null));
        echo "
\t\t\t</ul>
\t\t</div>
\t</div>
</nav><!--/nav-->";
    }

    // line 13
    public function getrender_menu($__links__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "links" => $__links__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 14
            echo "
";
            // line 15
            $context["base"] = "http://temp.poss.com.br";
            // line 16
            echo "
<li class=\"";
            // line 17
            if (($this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "id", array())) == ((isset($context["base"]) ? $context["base"] : null) . ""))) {
                echo "active";
            }
            echo "\"><a href=\"/\">HOME</a></li>
<li class=\"";
            // line 18
            if (($this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "id", array())) == ((isset($context["base"]) ? $context["base"] : null) . "/empresa"))) {
                echo "active";
            }
            echo "\"><a href=\"/empresa\">EMPRESA</a></li>
<li class=\"";
            // line 19
            if (($this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "id", array())) == ((isset($context["base"]) ? $context["base"] : null) . "/portfolio"))) {
                echo "active";
            }
            echo "\"><a href=\"/portfolio\">PORTFOLIO</a></li>
<li class=\"";
            // line 20
            if (($this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "id", array())) == ((isset($context["base"]) ? $context["base"] : null) . "/clientes"))) {
                echo "active";
            }
            echo "\"><a href=\"/clientes\">CLIENTES</a></li>
<li class=\"";
            // line 21
            if (($this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "id", array())) == ((isset($context["base"]) ? $context["base"] : null) . "/trabalhe-conosco"))) {
                echo "active";
            }
            echo "\"><a href=\"/trabalhe-conosco\">TRABALHE CONOSCO</a></li>
<li class=\"";
            // line 22
            if (($this->env->getExtension('Cms\Twig\Extension')->pageFilter($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "id", array())) == ((isset($context["base"]) ? $context["base"] : null) . "/contato"))) {
                echo "active";
            }
            echo "\"><a href=\"/contato\">CONTATO</a></li>
<li><a href=\"javascript:void(null)\" style=\"background-color: #007AA2 !important;color:#fff;\">(62) 3223-0375</a></li>

";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/nav.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 22,  105 => 21,  99 => 20,  93 => 19,  87 => 18,  81 => 17,  78 => 16,  76 => 15,  73 => 14,  61 => 13,  52 => 44,  42 => 39,  29 => 28,  27 => 27,  24 => 26,  21 => 12,  19 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Note: Only one levels of sublinks are supported by Bootstrap 3 #}
{% set
    links = {
\t    '': ['home', 'HOME'],
\t\t'empresa':   ['samples/empresa', 'EMPRESA'],
\t\t'portfolio': ['samples/portfolio', 'PORTFOLIO'],
\t\t'clientes':  ['samples/clientes', 'CLIENTES'],
        'trabalhe-conosco':   ['trabalhe_conosco', 'TRABALHE CONOSCO'],
        'contato':   ['samples/contato', 'CONTATO'],
    }
%}

{% macro render_menu(links) %}

{% set base='http://temp.poss.com.br' %}

<li class=\"{% if page.id|page == base ~ '' %}active{% endif %}\"><a href=\"/\">HOME</a></li>
<li class=\"{% if page.id|page == base ~ '/empresa' %}active{% endif %}\"><a href=\"/empresa\">EMPRESA</a></li>
<li class=\"{% if page.id|page == base ~ '/portfolio' %}active{% endif %}\"><a href=\"/portfolio\">PORTFOLIO</a></li>
<li class=\"{% if page.id|page == base ~ '/clientes' %}active{% endif %}\"><a href=\"/clientes\">CLIENTES</a></li>
<li class=\"{% if page.id|page == base ~ '/trabalhe-conosco' %}active{% endif %}\"><a href=\"/trabalhe-conosco\">TRABALHE CONOSCO</a></li>
<li class=\"{% if page.id|page == base ~ '/contato' %}active{% endif %}\"><a href=\"/contato\">CONTATO</a></li>
<li><a href=\"javascript:void(null)\" style=\"background-color: #007AA2 !important;color:#fff;\">(62) 3223-0375</a></li>

{% endmacro %}

{% import _self as nav %}


<nav class=\"navbar navbar-inverse\" role=\"banner\">
\t<div class=\"container\">
\t\t<div class=\"navbar-header\">
\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t<span class=\"sr-only\">Toggle navigation</span>
\t\t\t<span class=\"icon-bar\"></span>
\t\t\t<span class=\"icon-bar\"></span>
\t\t\t<span class=\"icon-bar\"></span>
\t\t\t</button>
\t\t\t<a class=\"navbar-brand\" href=\"{{ 'home'|page }}\"><img src=\"{{ 'assets/images/logo2.png'|theme }}\" alt=\"logo\"></a>
\t\t</div>
\t\t<div class=\"collapse navbar-collapse navbar-right\">
\t\t\t
\t\t\t<ul class=\"nav navbar-nav\">
\t\t\t\t{{ nav.render_menu(links) }}
\t\t\t</ul>
\t\t</div>
\t</div>
</nav><!--/nav-->", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/nav.htm", "");
    }
}
