<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/sustentabilidade.htm */
class __TwigTemplate_d776af8487950e49ec3f20126d9ac85b8a275e09c7271483496408e46bbc6848 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
            <div class=\"center wow fadeInDown\">
                <h2>GESTÃO SUSTENTÁVEL</h2>
                <p class=\"lead\" style=\"text-align: left;line-height: 30px;\">
                    Hoje a sustentabilidade é simplesmente um negócio inteligente mas um dos principais fatores da saúde financeira. É por isso que cada vez mais organizações estão à procura de empresas de mentalidade sustentável que tenham apliquem habilidades de gestão necessárias para competir em um mundo de recursos limitados e o aumento das expectativas dos consumidores.
                </p>
            </div>    

            <div class=\"partners\">
            </div>        
        </div><!--/.container-->";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/sustentabilidade.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">
            <div class=\"center wow fadeInDown\">
                <h2>GESTÃO SUSTENTÁVEL</h2>
                <p class=\"lead\" style=\"text-align: left;line-height: 30px;\">
                    Hoje a sustentabilidade é simplesmente um negócio inteligente mas um dos principais fatores da saúde financeira. É por isso que cada vez mais organizações estão à procura de empresas de mentalidade sustentável que tenham apliquem habilidades de gestão necessárias para competir em um mundo de recursos limitados e o aumento das expectativas dos consumidores.
                </p>
            </div>    

            <div class=\"partners\">
            </div>        
        </div><!--/.container-->", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/sustentabilidade.htm", "");
    }
}
