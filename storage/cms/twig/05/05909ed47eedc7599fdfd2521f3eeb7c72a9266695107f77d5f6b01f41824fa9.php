<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/trabalhe_conosco.htm */
class __TwigTemplate_d8ff33b2fd31891eac83d613d2252c6f4570ca9445d067860b9167672b9f781b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["records"] = $this->getAttribute((isset($context["vagas_list"]) ? $context["vagas_list"] : null), "records", array());
        // line 2
        $context["displayColumn"] = $this->getAttribute((isset($context["vagas_list"]) ? $context["vagas_list"] : null), "displayColumn", array());
        // line 3
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["vagas_list"]) ? $context["vagas_list"] : null), "noRecordsMessage", array());
        // line 4
        $context["detailsPage"] = $this->getAttribute((isset($context["vagas_list"]) ? $context["vagas_list"] : null), "detailsPage", array());
        // line 5
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["vagas_list"]) ? $context["vagas_list"] : null), "detailsKeyColumn", array());
        // line 6
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["vagas_list"]) ? $context["vagas_list"] : null), "detailsUrlParameter", array());
        // line 7
        echo "
<section id=\"about-us\" class=\"pagina_trabalhe_conosco\" style=\"padding-bottom:0;padding-top:220px;\">

    <div class=\"container-fluid\" style=\"background-color:#f2f2f2;padding-top:40px;\">

\t\t<div class=\"center wow fadeInDown\">
\t\t\t<h2><img src=\"";
        // line 13
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Trabalhe Conosco</h2>
\t\t\t<p>Conhecimento Técnico, Qualidade, Agilidade e confiabilidade dos serviços prestados.</p>\t\t
\t\t</div>

\t\t<div class=\"row wow fadeInDown\" style=\"padding: 40px;\">
\t\t\t
\t\t\t<div class=\"col-md-10  col-md-offset-2 formulario\">
\t\t\t
\t\t\t\t<div class=\"col-md-6\" style=\"padding-right: 80px;\">
                    ";
        // line 22
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("trabalheconosco"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 23
        echo "\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-6\">

\t\t\t\t\t<h2 style=\"color: #28a0ae;\">VAGAS DISPONÍVEIS</h2>

\t\t\t\t\t<ul class=\"record-list\" style=\"padding: 0;padding-left: 20px;\">
\t\t\t\t\t    ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 31
            echo "\t\t\t\t\t        ";
            ob_start();
            // line 32
            echo "\t\t\t\t\t            <li> 
\t\t\t\t\t               ";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "titulo", array()), "html", null, true);
            echo " 
\t\t\t\t\t               <a href=\"javascript:alert('Quantidade: ";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "quantidade", array()), "html", null, true);
            echo " \\nDescrição: ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "descricao", array()), "html", null, true);
            echo "')\">Detalhe</a>
\t\t\t\t\t            </li>
\t\t\t\t\t        ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 37
            echo "\t\t\t\t\t    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "\t\t\t\t\t</ul>
\t\t\t\t</div>

\t\t\t</div>

\t\t</div>

\t</div>

</section>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/trabalhe_conosco.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 38,  86 => 37,  78 => 34,  74 => 33,  71 => 32,  68 => 31,  64 => 30,  55 => 23,  51 => 22,  39 => 13,  31 => 7,  29 => 6,  27 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set records = vagas_list.records %}
{% set displayColumn = vagas_list.displayColumn %}
{% set noRecordsMessage = vagas_list.noRecordsMessage %}
{% set detailsPage = vagas_list.detailsPage %}
{% set detailsKeyColumn = vagas_list.detailsKeyColumn %}
{% set detailsUrlParameter = vagas_list.detailsUrlParameter %}

<section id=\"about-us\" class=\"pagina_trabalhe_conosco\" style=\"padding-bottom:0;padding-top:220px;\">

    <div class=\"container-fluid\" style=\"background-color:#f2f2f2;padding-top:40px;\">

\t\t<div class=\"center wow fadeInDown\">
\t\t\t<h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Trabalhe Conosco</h2>
\t\t\t<p>Conhecimento Técnico, Qualidade, Agilidade e confiabilidade dos serviços prestados.</p>\t\t
\t\t</div>

\t\t<div class=\"row wow fadeInDown\" style=\"padding: 40px;\">
\t\t\t
\t\t\t<div class=\"col-md-10  col-md-offset-2 formulario\">
\t\t\t
\t\t\t\t<div class=\"col-md-6\" style=\"padding-right: 80px;\">
                    {% component 'trabalheconosco' %}
\t\t\t\t</div>

\t\t\t\t<div class=\"col-md-6\">

\t\t\t\t\t<h2 style=\"color: #28a0ae;\">VAGAS DISPONÍVEIS</h2>

\t\t\t\t\t<ul class=\"record-list\" style=\"padding: 0;padding-left: 20px;\">
\t\t\t\t\t    {% for record in records %}
\t\t\t\t\t        {% spaceless %}
\t\t\t\t\t            <li> 
\t\t\t\t\t               {{ record.titulo }} 
\t\t\t\t\t               <a href=\"javascript:alert('Quantidade: {{ record.quantidade }} \\nDescrição: {{ record.descricao }}')\">Detalhe</a>
\t\t\t\t\t            </li>
\t\t\t\t\t        {% endspaceless %}
\t\t\t\t\t    {% endfor %}
\t\t\t\t\t</ul>
\t\t\t\t</div>

\t\t\t</div>

\t\t</div>

\t</div>

</section>", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/trabalhe_conosco.htm", "");
    }
}
