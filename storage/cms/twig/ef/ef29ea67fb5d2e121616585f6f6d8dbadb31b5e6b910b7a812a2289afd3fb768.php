<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/intro.htm */
class __TwigTemplate_f1a15ea2c0d4c666c3edd0fa2884afc6b8fe79e9ddf770ccf38bce639ca63c0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">

            <div class=\"col-sm-8 col-sm-offset-2 colcenter wow fadeInDown\" style=\"text-align: center;\">
                <h2>Temos o que você precisa</h2>
                <p class=\"lead\">Temos equipes de profissionais altamente qualificados para prestar os serviços de: consultoria, elaboração de projetos,execução e acompanhamento de obras.</p>
            </div>    
      
        </div><!--/.container-->";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/intro.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">

            <div class=\"col-sm-8 col-sm-offset-2 colcenter wow fadeInDown\" style=\"text-align: center;\">
                <h2>Temos o que você precisa</h2>
                <p class=\"lead\">Temos equipes de profissionais altamente qualificados para prestar os serviços de: consultoria, elaboração de projetos,execução e acompanhamento de obras.</p>
            </div>    
      
        </div><!--/.container-->", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/intro.htm", "");
    }
}
