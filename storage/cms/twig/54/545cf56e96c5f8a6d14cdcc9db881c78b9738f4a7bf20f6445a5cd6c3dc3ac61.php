<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/google_maps.htm */
class __TwigTemplate_53d5fd96baf77189a290a4bd881579bd7b52330f29363236c9a81e29dd79aac6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"contact-info\">
        <div class=\"gmap-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-5 text-center\">
                        <div class=\"gmap\">
       <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7650.043530030036!2d-49.27185487909778!3d-16.671189014547846!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef3e6c657417f%3A0x14eab61b5296e44!2sR.+17-A%2C+896+-+St.+Aeroporto%2C+Goi%C3%A2nia+-+GO%2C+74070-100!5e0!3m2!1spt-BR!2sbr!4v1500297351365\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class=\"col-sm-7 map-content\" style=\"padding-top:50px;\">
                        <ul class=\"row\">
                            <li class=\"col-sm-6\">
                                <address>
                                    <h4>POSS ENGENHARIA</h4>
                                    <p><i class=\"fa fa-location-arrow\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i></i> Av. Pires Fernandes, Nº. 39,<br/>Sala 01, St. Aeroporto,<br/>Goiânia - GO - CEP: 74.070-030<br/>
                                    <a target=\"_blank\" class=\"linkhoverwhite\" href=\"https://goo.gl/maps/sFpMGtY4xdG2\">Como Chegar</a>
                                    </p>
                                    <p><i class=\"fa fa-phone\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i> +55 (62) 3223-0375<br><br>
                                    <i class=\"fa fa-envelope\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i> Email: contato@poss.com.br</p>
                                </address>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->

<!-- <div class=\"container-fluid wow fadeInDown\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
    <div class=\"row\">
        <div id=\"map_google\" style=\"height: 400px;\"></div>
    </div>
</div> -->
    <script>

      // var marker,map,possLatLng;
      // var iconMarker = '";
        // line 38
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/marker3.png");
        echo "';

      // function initMap() {

      //   possLatLng = {lat: -16.671223, lng: -49.267340};

      //   // Create a map object and specify the DOM element for display.
      //   map = new google.maps.Map(document.getElementById('map_google'), {
      //     center: possLatLng,
      //     scrollwheel: false,
      //     zoom: 15
      //   });

      //   marker = new google.maps.Marker({
      //     position: possLatLng,
      //     map: map,
      //     title: 'Poss Engenharia',
      //     icon: iconMarker
      //   });

      //   toggleBounce();
      //   marker.addListener('click', toggleBounce);

      // }

      // function toggleBounce() {
      //   if (marker.getAnimation() !== null) {
      //     marker.setAnimation(null);
      //   } else {
      //     marker.setAnimation(google.maps.Animation.BOUNCE);
      //   }
      // }

    </script>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/google_maps.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 38,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"contact-info\">
        <div class=\"gmap-area\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-sm-5 text-center\">
                        <div class=\"gmap\">
       <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7650.043530030036!2d-49.27185487909778!3d-16.671189014547846!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef3e6c657417f%3A0x14eab61b5296e44!2sR.+17-A%2C+896+-+St.+Aeroporto%2C+Goi%C3%A2nia+-+GO%2C+74070-100!5e0!3m2!1spt-BR!2sbr!4v1500297351365\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class=\"col-sm-7 map-content\" style=\"padding-top:50px;\">
                        <ul class=\"row\">
                            <li class=\"col-sm-6\">
                                <address>
                                    <h4>POSS ENGENHARIA</h4>
                                    <p><i class=\"fa fa-location-arrow\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i></i> Av. Pires Fernandes, Nº. 39,<br/>Sala 01, St. Aeroporto,<br/>Goiânia - GO - CEP: 74.070-030<br/>
                                    <a target=\"_blank\" class=\"linkhoverwhite\" href=\"https://goo.gl/maps/sFpMGtY4xdG2\">Como Chegar</a>
                                    </p>
                                    <p><i class=\"fa fa-phone\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i> +55 (62) 3223-0375<br><br>
                                    <i class=\"fa fa-envelope\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i> Email: contato@poss.com.br</p>
                                </address>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->

<!-- <div class=\"container-fluid wow fadeInDown\" data-wow-duration=\"1000ms\" data-wow-delay=\"600ms\">
    <div class=\"row\">
        <div id=\"map_google\" style=\"height: 400px;\"></div>
    </div>
</div> -->
    <script>

      // var marker,map,possLatLng;
      // var iconMarker = '{{ \"assets/images/marker3.png\"|theme }}';

      // function initMap() {

      //   possLatLng = {lat: -16.671223, lng: -49.267340};

      //   // Create a map object and specify the DOM element for display.
      //   map = new google.maps.Map(document.getElementById('map_google'), {
      //     center: possLatLng,
      //     scrollwheel: false,
      //     zoom: 15
      //   });

      //   marker = new google.maps.Marker({
      //     position: possLatLng,
      //     map: map,
      //     title: 'Poss Engenharia',
      //     icon: iconMarker
      //   });

      //   toggleBounce();
      //   marker.addListener('click', toggleBounce);

      // }

      // function toggleBounce() {
      //   if (marker.getAnimation() !== null) {
      //     marker.setAnimation(null);
      //   } else {
      //     marker.setAnimation(google.maps.Animation.BOUNCE);
      //   }
      // }

    </script>", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/partials/home/google_maps.htm", "");
    }
}
