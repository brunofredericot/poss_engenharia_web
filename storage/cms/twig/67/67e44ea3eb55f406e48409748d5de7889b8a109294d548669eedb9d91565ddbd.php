<?php

/* /var/www/poss-engenharia/web/plugins/bruno/trabalheconosco/components/trabalheconosco/default.htm */
class __TwigTemplate_cb852dee858912eefbd659949099f2104cc4f90a9cdfabff547f4daf84d7f39f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "\t<h2>Preencha o formulário abaixo</h2>
\t<hr/>
\t<form data-request=\"onSend\">

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<label>NOME COMPLETO</label>
\t\t\t<input type=\"text\" class=\"form-control\" name=\"nome\">
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-6\">
\t\t\t<label>E-MAIL</label>
\t\t\t<input type=\"text\" class=\"form-control\" name=\"email\">
\t\t</div>
\t\t<div class=\"col-md-6\">
\t\t\t<label>TELEFONE</label>
\t\t\t<input type=\"text\" class=\"form-control\" name=\"telefone\">
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<label>VAGA DESEJADA</label>
\t\t\t<input type=\"text\" class=\"form-control\" name=\"vaga\">
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<label>MENSAGEM</label>
\t\t\t<textarea class=\"form-control\" style=\"min-height: 100px;\" name=\"mensagem\"></textarea>
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<label>Anexo:</label><input type=\"file\" name=\"curriculo\">
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<button class=\"btn btn-default btn-block\" type=\"submit\">ENVIAR</button>
\t\t</div>
\t</div>

\t</form>";
    }

    public function getTemplateName()
    {
        return "/var/www/poss-engenharia/web/plugins/bruno/trabalheconosco/components/trabalheconosco/default.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("\t<h2>Preencha o formulário abaixo</h2>
\t<hr/>
\t<form data-request=\"onSend\">

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<label>NOME COMPLETO</label>
\t\t\t<input type=\"text\" class=\"form-control\" name=\"nome\">
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-6\">
\t\t\t<label>E-MAIL</label>
\t\t\t<input type=\"text\" class=\"form-control\" name=\"email\">
\t\t</div>
\t\t<div class=\"col-md-6\">
\t\t\t<label>TELEFONE</label>
\t\t\t<input type=\"text\" class=\"form-control\" name=\"telefone\">
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<label>VAGA DESEJADA</label>
\t\t\t<input type=\"text\" class=\"form-control\" name=\"vaga\">
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<label>MENSAGEM</label>
\t\t\t<textarea class=\"form-control\" style=\"min-height: 100px;\" name=\"mensagem\"></textarea>
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<label>Anexo:</label><input type=\"file\" name=\"curriculo\">
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"padding-top: 10px;\">
\t\t<div class=\"col-md-12\">
\t\t\t<button class=\"btn btn-default btn-block\" type=\"submit\">ENVIAR</button>
\t\t</div>
\t</div>

\t</form>", "/var/www/poss-engenharia/web/plugins/bruno/trabalheconosco/components/trabalheconosco/default.htm", "");
    }
}
