<?php

/* /var/www/html/in_poss_temp/plugins/bruno/contato/components/contactform/default.htm */
class __TwigTemplate_f0a466443ec0964d1f2c31a396d81b90bece5f3dec5a6863186a37dbfc1c0005 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"about-us\" style=\"background: #f2f2f2;\">

    <div class=\"container\">

        <div class=\"center wow fadeInDown\">
            <h2><img src=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Entre em contato conosco</h2>
        </div>

        <div class=\"row wow fadeInDown\" style=\"padding: 40px;\">
            
            <div class=\"col-md-10  col-md-offset-2 formulario\">
            
                <div class=\"col-md-6\" style=\"padding-right: 80px;\">

                    <h2>Preencha o formulário abaixo</h2>
                    <hr/>

                    <form data-request=\"\">

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>NOME COMPLETO</label>
                                <input type=\"text\" class=\"form-control\">
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-6\">
                                <label>E-MAIL</label>
                                <input type=\"text\" class=\"form-control\">
                            </div>
                            <div class=\"col-md-6\">
                                <label>TELEFONE</label>
                                <input type=\"text\" class=\"form-control\">
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>ASSUNTO</label>
                                <input type=\"text\" class=\"form-control\">
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>MENSAGEM</label>
                                <textarea class=\"form-control\" style=\"min-height: 100px;\"></textarea>
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <button class=\"btn btn-default btn-block\">ENVIAR</button>
                            </div>
                        </div>

                    </form>


                </div>

                <div class=\"col-md-6\">

                    <h2 style=\"color: #28a0ae;\"><img src=\"";
        // line 65
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/simbolo.png");
        echo "\" alt=\"\" style=\"width: 50px;\"> POSS ENGENHARIA</h2>
                    <p style=\"font-style: italic;color: #bbb;padding-left: 20px;\">Uma grande satifação pode atendar</p>
                   
                    <p style=\"padding-top: 20px;padding-left: 20px;\">

                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                        Av. Pires Fernandes, Nº. 39,<br/>Sala 01, St. Aeroporto,<br/>Goiânia - GO - CEP: 74.070-030<br/>
                        [ <a target=\"_blank\" class=\"linkhoverwhite\" href=\"https://goo.gl/maps/sFpMGtY4xdG2\">Como Chegar</a> ]<br/><br/>

                        <i class=\"fa fa-phone\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                         +55 (62) 3223-0375<br/><br/>
                        
                        <i class=\"fa fa-envelope\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                        info@domain.com

                    </p>

                </div>

            </div>

        </div>

    </div>

</section>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/plugins/bruno/contato/components/contactform/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 65,  26 => 6,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"about-us\" style=\"background: #f2f2f2;\">

    <div class=\"container\">

        <div class=\"center wow fadeInDown\">
            <h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Entre em contato conosco</h2>
        </div>

        <div class=\"row wow fadeInDown\" style=\"padding: 40px;\">
            
            <div class=\"col-md-10  col-md-offset-2 formulario\">
            
                <div class=\"col-md-6\" style=\"padding-right: 80px;\">

                    <h2>Preencha o formulário abaixo</h2>
                    <hr/>

                    <form data-request=\"\">

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>NOME COMPLETO</label>
                                <input type=\"text\" class=\"form-control\">
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-6\">
                                <label>E-MAIL</label>
                                <input type=\"text\" class=\"form-control\">
                            </div>
                            <div class=\"col-md-6\">
                                <label>TELEFONE</label>
                                <input type=\"text\" class=\"form-control\">
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>ASSUNTO</label>
                                <input type=\"text\" class=\"form-control\">
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <label>MENSAGEM</label>
                                <textarea class=\"form-control\" style=\"min-height: 100px;\"></textarea>
                            </div>
                        </div>

                        <div class=\"row\" style=\"padding-top: 10px;\">
                            <div class=\"col-md-12\">
                                <button class=\"btn btn-default btn-block\">ENVIAR</button>
                            </div>
                        </div>

                    </form>


                </div>

                <div class=\"col-md-6\">

                    <h2 style=\"color: #28a0ae;\"><img src=\"{{ 'assets/images/simbolo.png'|theme }}\" alt=\"\" style=\"width: 50px;\"> POSS ENGENHARIA</h2>
                    <p style=\"font-style: italic;color: #bbb;padding-left: 20px;\">Uma grande satifação pode atendar</p>
                   
                    <p style=\"padding-top: 20px;padding-left: 20px;\">

                        <i class=\"fa fa-location-arrow\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                        Av. Pires Fernandes, Nº. 39,<br/>Sala 01, St. Aeroporto,<br/>Goiânia - GO - CEP: 74.070-030<br/>
                        [ <a target=\"_blank\" class=\"linkhoverwhite\" href=\"https://goo.gl/maps/sFpMGtY4xdG2\">Como Chegar</a> ]<br/><br/>

                        <i class=\"fa fa-phone\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                         +55 (62) 3223-0375<br/><br/>
                        
                        <i class=\"fa fa-envelope\" aria-hidden=\"true\" style=\"color:#28a0ae;\"></i>
                        info@domain.com

                    </p>

                </div>

            </div>

        </div>

    </div>

</section>", "/var/www/html/in_poss_temp/plugins/bruno/contato/components/contactform/default.htm", "");
    }
}
