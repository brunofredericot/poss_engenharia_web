<?php

/* /var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/instaladora.htm */
class __TwigTemplate_4093513931cd72e59cb01cdbd496c5b542f6adcf55a604865a567d806473536e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"about-us\">
<div class=\"container\">
   <div class=\"center wow fadeInDown\">
        <h2><img src=\"";
        // line 4
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/leaf.png");
        echo "\" alt=\"\"> Instaladora</h2>
        <p class=\"lead\" style=\"width: 70%;margin:auto;\">A Poss Engenharia oferece os serviços de instaladora.</p>
    </div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/instaladora.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"about-us\">
<div class=\"container\">
   <div class=\"center wow fadeInDown\">
        <h2><img src=\"{{ 'assets/images/leaf.png'|theme }}\" alt=\"\"> Instaladora</h2>
        <p class=\"lead\" style=\"width: 70%;margin:auto;\">A Poss Engenharia oferece os serviços de instaladora.</p>
    </div>
</div>
</div>", "/var/www/html/in_poss_temp/themes/jtherczeg-corlate/pages/instaladora.htm", "");
    }
}
